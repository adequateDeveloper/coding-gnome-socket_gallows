# SocketGallows

[![pipeline status](https://gitlab.com/adequateDeveloper/coding-gnome-socket_gallows/badges/master/pipeline.svg)](https://gitlab.com/adequateDeveloper/coding-gnome-socket_gallows/commits/master)

[Following the Dave Thomas Elixir for Programmers course](https://coding-gnome.thinkific.com/courses/elixir-for-programmers)

## Features

* ExDoc package for project documentation
* Credo package for static code analysis
* Coveralls package for code coverage report
* Dialyzir package for static API analysis
* Hangman package pulled from GitHub
* Exports private functions for testing


To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
